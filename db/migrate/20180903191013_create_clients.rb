class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :lastname
      t.string :ci
      t.string :mobile
      t.string :phone

      t.timestamps
    end
  end
end
