class CreateFreights < ActiveRecord::Migration[5.2]
  def change
    create_table :freights do |t|
      t.integer :kind
      t.integer :weight
      t.text :description
      t.references :shipping, foreign_key: true

      t.timestamps
    end
  end
end
