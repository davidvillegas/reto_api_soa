class CreateShippings < ActiveRecord::Migration[5.2]
  def change
    create_table :shippings do |t|
      t.date :order_date
      t.decimal :base_price, precision: 3, scale: 2
      t.date :pickup_date
      t.integer :status
      t.references :receiver, foreign_key: {to_table: :clients}, null: false
      t.references :sender, foreign_key: {to_table: :clients}, null: false
      t.references :office, foreign_key: true

      t.timestamps
    end
  end
end
