# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_04_030007) do

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "lastname"
    t.string "ci"
    t.string "mobile"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "freights", force: :cascade do |t|
    t.integer "kind"
    t.integer "weight"
    t.text "description"
    t.integer "shipping_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shipping_id"], name: "index_freights_on_shipping_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offices", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.text "address"
    t.integer "state_id", null: false
    t.integer "city_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_offices_on_city_id"
    t.index ["state_id"], name: "index_offices_on_state_id"
  end

  create_table "paths", force: :cascade do |t|
    t.date "date"
    t.date "arrival_date"
    t.decimal "price", precision: 3, scale: 2
    t.integer "status"
    t.integer "shipping_id"
    t.integer "office_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["office_id"], name: "index_paths_on_office_id"
    t.index ["shipping_id"], name: "index_paths_on_shipping_id"
  end

  create_table "rates", force: :cascade do |t|
    t.decimal "price", precision: 3, scale: 2
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["destination_id"], name: "index_rates_on_destination_id"
    t.index ["origin_id"], name: "index_rates_on_origin_id"
  end

  create_table "shippings", force: :cascade do |t|
    t.date "order_date"
    t.decimal "base_price", precision: 3, scale: 2
    t.date "pickup_date"
    t.integer "status"
    t.integer "receiver_id", null: false
    t.integer "sender_id", null: false
    t.integer "office_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["office_id"], name: "index_shippings_on_office_id"
    t.index ["receiver_id"], name: "index_shippings_on_receiver_id"
    t.index ["sender_id"], name: "index_shippings_on_sender_id"
  end

end
