=begin

Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 138.99, status: 0, receiver_id: 2, sender_id: 1, office_id: 3)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 331.99, status: 0, receiver_id: 3, sender_id: 2, office_id: 8)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 575.99, status: 0, receiver_id: 4, sender_id: 3, office_id: 18)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 457.99, status: 0, receiver_id: 5, sender_id: 4, office_id: 10)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 479.99, status: 0, receiver_id: 1, sender_id: 5, office_id: 1)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 238.99, status: 0, receiver_id: 1, sender_id: 2, office_id: 23)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 723.99, status: 0, receiver_id: 2, sender_id: 4, office_id: 14)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 172.99, status: 0, receiver_id: 4, sender_id: 1, office_id: 19)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 632.99, status: 0, receiver_id: 2, sender_id: 5, office_id: 20)
Shipping.create(order_date: Faker::Time.between(1.month.ago, 1.week.ago), base_price: 332.99, status: 0, receiver_id: 3, sender_id: 4, office_id: 13)

=end
