Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :clients
  resources :shippings do
    collection do
      get :search, to: 'shipping_searches#index'
    end
  end
  resources :paths
  resources :freights

  #otro metodo para buscar estaus con método POST
  post 'shippings/status', to: 'shippings#packge_pendings'

end
