# README
Reto para hacer en clase

Implementar el sistema (API SOA) de control para la empresa de envío express de encomiendas MVR que, a nivel nacional, ofrece los siguientes servicios:

1) Envíos prepagados de documentos
2) Envíos prepagados de paquetes (hasta 5kg de peso)

Proceso de envío de encomienda

1) Un cliente acude a una sede de MVR con el paquete
2) Se presupuesta el paquete o documento (tipo, descripción, peso, sede de MVR destino, datos del destinatario).
3) Si el cliente aprueba el presupuesto, cancela el costo del envío.
4) Se busca al cliente en el sistema.
5) Si no existe, se registra el cliente.
6) Se aprueba el envío ya cancelado: se registra en el sistema.
7) Cuando el paquete llega a la sede destino y el destinatario acude a retirarlo, debe indicar el N° de envío para que su paquete pueda ser encontrado dentro de la sede y le sea entregado. La entrega del paquete al destinatario debe registrarse en el envío.
8) Es posible que el cliente remitente cambie la sede de destino del paquete para que el receptor pueda retirarlo en esta última, lo que genera un cargo adicional.

Requerimientos

1) Registrar un cliente remitente para que pueda enviar encomiendas
2) Registrar la orden de envío de encomiendas a otra sede de MVR.
3) Registrar la recepción (llegada) de una encomienda en una sede.
4) Registrar la entrega de una encomienda al receptor final.
5) Verificar estado de un envío.
6) Ver envíos pendientes por despachar.
7) Ver envíos pendientes por entregar.
8) Registrar la redirección de una sede de MVR a otra de una encomienda a petición del cliente remitente.
9) Cancelar los cargos adicionales generados por los cambios de sede de destino.
