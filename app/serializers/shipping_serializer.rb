class ShippingSerializer < ActiveModel::Serializer
  attributes :id, :order_date, :base_price, :status, :pickup_date, :total_pay

  has_one :sender
  has_one :receiver
  has_one :office
  has_many :paths
  has_one :freight
end
