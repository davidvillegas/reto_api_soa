class FreightSerializer < ActiveModel::Serializer
  attributes :shipping_id, :kind, :weight, :description

  has_one :shipping
end
