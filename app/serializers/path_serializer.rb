class PathSerializer < ActiveModel::Serializer
  attributes :shipping_id, :price, :arrival_date, :status, :office_id

  has_one :shipping

end
