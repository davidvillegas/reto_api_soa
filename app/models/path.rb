class Path < ApplicationRecord
  belongs_to :shipping
  belongs_to :office

  enum status: [:current, :deflected]
end
