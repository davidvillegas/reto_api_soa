class Location < ApplicationRecord
  has_many :state_offices, class_name: "Office", foreign_key: :state_id
  has_many :city_offices, class_name: "Office", foreign_key: :city_id

  enum status: [:state, :city]
end
