class Shipping < ApplicationRecord
  belongs_to :receiver, foreign_key: :receiver_id, class_name: "Client"
  belongs_to :sender, foreign_key: :sender_id, class_name: "Client"
  belongs_to :office
  has_one :freight
  has_many :paths

  enum status: [:pending, :sent, :received, :delivered]

  def total_pay
    t = 0
    paths.each do |i|
      t += i.price
    end
    p "Cargo Adicional por Desvío de Paquete: #{t}"
  end

end
