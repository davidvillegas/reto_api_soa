class Client < ApplicationRecord
  has_many :sent_shippings, class_name: "Shipping", foreign_key: :sender_id
  has_many :received_shippings, class_name: "Shipping", foreign_key: :receiver_id
end
