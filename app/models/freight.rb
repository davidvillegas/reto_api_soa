class Freight < ApplicationRecord
  belongs_to :shipping

  enum status: [:package, :document]
end
