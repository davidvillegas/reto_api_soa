class FreightsController < ApplicationController

  def index
    @freights = Freight.all
    render json: @freights
  end

end
