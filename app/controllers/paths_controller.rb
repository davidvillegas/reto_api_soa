class PathsController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    @paths = Path.all
    render json: @paths
  end

  # Con este metodo creamos un nuevo path y un reenvío en tal de ser solicitado
  def create
    #@path = Path.create(create_params)
    @path = Path.new
    @path.date = DateTime.now
    @path.price = path_params[:price]
    @path.status = path_params[:status]
    @shipping = Shipping.find path_params[:shipping_id]
    @path.shipping = @shipping
    @shipping.status = "sent"
    @office = Office.find path_params[:office_id]
    @path.office = @office
    @path.save
    @shipping.save

    render json: @path
  end

  #Verificamos el estatus de un envio del paquete por ruta
  def show
      @path = Path.find_by(id: params[:id])
      if @path
        render json: @path
      else
        render json: { error: 'Path does not exist' }
      end
  end

  #registramos la llegada de un paquete en una sede
  def update
    @path = Path.find_by(id: params[:id])
    @path.arrival_date = DateTime.now
    @path.price = path_update_params[:price]
    @path.status = path_update_params[:status]
    @shipping = Shipping.find path_update_params[:shipping_id]
    @path.shipping = @shipping
    @shipping.status = "received"
    @office = Office.find path_update_params[:office_id]
    @path.office = @office
    @shipping.save
    render json: @path.update(path_update_params) ? @path : { error: 'Record could not be updated' }
  end

  private

  def path_params
    params.require(:path).permit(:price, :status, :shipping_id, :office_id)
  end

  def path_update_params
    params.require(:path).permit(:price, :status, :shipping_id, :office_id)
  end
end
