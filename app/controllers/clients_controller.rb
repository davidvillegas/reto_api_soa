class ClientsController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    @clients = Client.all
    render json: @clients unless @clients.empty?
    #render json: !@clients.empty? ? @clients : {error: "No Existe"}
  end

  def create
    # begin
      @client = Client.create(client_params)
      render json: @client
    # rescue ActiveRecord::RecordNotCreated => e
    #   render json: { error: e.message }
    # end
  end

  private

  def client_params
    params.require(:client).permit(:name, :lastname, :ci, :mobile, :phone)
  end
end
