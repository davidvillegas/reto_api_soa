class ShippingsController < ApplicationController
  protect_from_forgery with: :null_session

  def index
    @shippings = Shipping.all
    render json: @shippings
  end

  def create
    @shipping = Shipping.new
    @shipping.status = "pending"
    @shipping.order_date = DateTime.now
    @shipping.base_price = shipping_params[:base_price]
    @sender = Client.find shipping_params[:sender_id]
    @shipping.sender = @sender
    @receiver = Client.find shipping_params[:receiver_id]
    @shipping.receiver = @receiver
    @office = Office.find shipping_params[:office_id]
    @shipping.office = @office
    @shipping.save

    render json: @shipping
  end

  #Verificamos el estatus de un envio completo con sus rutas y tipo de paquete
  def show
    @shipping = Shipping.find(params[:id])
    if @shipping
      render json: @shipping
    else
      render json: { error: 'Shipping does not exist' }
    end
  end

  #Con este método registramos la entrega de una encomienda a un receptor final
  def update
    @shipping = Shipping.find_by(id: params[:id])
    @shipping.pickup_date = shipping_update_params[:pickup_date]
    @shipping.status = "delivered"
    render json: @shipping.update(shipping_update_params) ? @shipping : { error: 'Record could not be updated' }
  end

  # Con este método consultamos cualquier estatus del shipping indicado por parámetro
  def packge_pendings
    @package = Shipping.where(status: params[:status])
    if @package
      render json: @package
    else
      render json: { error: 'Shipping Not Found' }
    end
  end

  private

  def shipping_params
    params.require(:shipping).permit(:base_price, :receiver_id, :sender_id, :office_id)
  end

  def shipping_update_params
    params.require(:shipping).permit(:pickup_date)
  end

end
